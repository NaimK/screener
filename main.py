# %%
import os
from dotenv import load_dotenv
from screener.services.datafeed.datafeed_handler import DatafeedHandler
from screener.strategy.drawer import Drawer

load_dotenv()

# Connect to Datafeed handler (using trading view data)
datafeed_handler = DatafeedHandler(
    username=os.getenv('TV_USERNAME'),
    password=os.getenv('TV_PASSWORD')
)
ticker = 'COLR'
market = 'EURONEXT'
# Get stock data
datafeed_handler.prepare_stock(ticker, market)
# Draw stock indicators
drawer = Drawer(datafeed_handler)
drawer.draw_indicators()

# %%
