import numpy as np
import pandas_ta as ta
from tvDatafeed import TvDatafeed

from screener.enums.datafeed_interval import DatafeedInterval


class DatafeedHandler:
    def __init__(self, username: str, password: str):
        self._password = password
        self._username = username
        self._datafeed = None
        self._diffs = None
        self._market = None
        self._OHLCs = None
        self._ticker = None
        self._signs = None
        self._stock_dataframe = None
        self._x_data = None
        self._y_data = None
        self._connect_datafeed()

    # GETTERS & SETTERS

    @property
    def datafeed(self):
        return self._datafeed

    @property
    def ohlc(self):
        return self._OHLCs

    @property
    def signs(self):
        return self._signs

    @property
    def stock(self):
        return {'ticker': self._ticker, 'market': self._market}

    @stock.setter
    def stock(self, values):
        try:
            ticker, market = values
        except ValueError:
            raise ValueError(
                "Pass an iterable with two items (first is the ticker, second is the market)")
        self._ticker = ticker
        self._market = market

    @property
    def stock_dataframe(self):
        return self._stock_dataframe

    # METHODS

    # PUBLIC METHODS

    def prepare_stock(self, ticker: str, market: str):
        """"Prepares stock data by calling internal methods to 
        get stock history and to arrange it."""

        self.stock = (ticker, market)
        self._get_stock_history()
        self._arrange_stock_data()

    # PRIVATE METHODS

    def _get_stock_history(self, interval='days1', bars=200):
        """"Gets stock history with provided interval and bars count"""

        try:
            self._stock_dataframe = self._datafeed.get_hist(
                self._ticker, self._market, DatafeedInterval[interval], n_bars=bars)
            self._stock_dataframe.reset_index(inplace=True)
            return self._stock_dataframe
        except KeyError:
            print(f"${interval} is not a valid DatafeedInterval interval")

    def _arrange_stock_data(self):
        """"Arranges stock data for further manipulation"""

        if self._stock_dataframe is not None:
            self._add_rsi()
            self._arrange_xy_data()
            self._arrange_ohlc_data()
            self._arrange_diffs()
            self._arrange_signs()
        else:
            raise Exception(
                "Can't arrange OHLC because _stock_dataframe is not set. Try calling _get_stock_history() first.")

    def _add_rsi(self):
        """Adds RSI to dataframe"""
        self._stock_dataframe['RSI'] = self._stock_dataframe.ta.rsi(length=14)

    def _arrange_diffs(self):
        """"Stores data about the price evolution"""
        self._diffs = {
            'opens': np.diff(self._OHLCs['opens']),
            'highs': np.diff(self._OHLCs['highs']),
            'lows': np.diff(self._OHLCs['lows']),
        }

    def _arrange_ohlc_data(self):
        """"Arranges OHLC (Open, High, Low, Close) into an attribute"""

        self._OHLCs = {
            'opens': self._stock_dataframe['open'],
            'highs': self._stock_dataframe['high'],
            'lows': self._stock_dataframe['low'],
            'closes': self._stock_dataframe['close'],
        }

    def _arrange_signs(self):
        """ Arranges price evolution signs

        -1 means that the price is going down\n
        1 means that the price is going up
        """
        self._signs = {
            'opens': np.sign(self._diffs['opens']),
            'highs': np.sign(self._diffs['highs']),
            'lows': np.sign(self._diffs['lows']),
        }

    def _arrange_xy_data(self):
        self._x_data = self._stock_dataframe.index.tolist()
        self._y_data = self._stock_dataframe['low']

    def _connect_datafeed(self):
        """Connects to Trading View datafeed"""

        self._datafeed = TvDatafeed(self._username, self._password)
