from enum import Enum


class DatafeedInterval(Enum):
    "Enumerates ticker intervals for chart plotting timeframe"

    minutes1 = "1"
    minutes3 = "3"
    minutes5 = "5"
    minutes15 = "15"
    minutes30 = "30"
    minutes45 = "45"
    hours1 = "1H"
    hours2 = "2H"
    hours3 = "3H"
    hours4 = "4H"
    days1 = "1D"
    weeks1 = "1W"
    months1 = "1M"
