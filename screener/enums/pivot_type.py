from enum import Enum


class PivotType(Enum):
    "Enumerates pivot points used by divergence identifiers"

    low = 1
    high = 2
    lowHigh = 3
    none = 0
