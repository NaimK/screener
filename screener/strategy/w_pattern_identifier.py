from pydoc import doc
import numpy as np

from screener.services.datafeed.datafeed_handler import DatafeedHandler


class WPatternIdentifier:
    """This class purpose is to identify W pattern"""

    def __init__(self, datafeed_handler: DatafeedHandler):
        self._datafeed_handler = datafeed_handler
        self._markers_index = None
        self._pattern_mean_points = None
        self._pattern_points = None
        self._w_pattern = None

    # GETTERS

    @property
    def pattern_points(self):
        return self._pattern_points

    # METHODS

    def look_for_pattern(self):
        """Calls internal methods to identify W patterns using provided DatafeedHandler"""

        self._set_price_line()
        self._set_markers_and_mean_points()

    def _set_price_line(self):
        """Sets the price line according to W pattern requirements"""

        dfh = self._datafeed_handler
        points = np.array([])

        for idx, x in np.ndenumerate(dfh.signs['opens']):
            index = idx[0]
            previous_index = index - 1 if index > 0 else index

            open_sign = x
            low_sign = dfh.signs['lows'][index]
            low_price = dfh.ohlc['lows'][index]
            open_price = dfh.ohlc['opens'][index]
            open_price_previous = dfh.ohlc['opens'][previous_index]
            close_price = dfh.ohlc['closes'][index]
            close_price_previous = dfh.ohlc['closes'][previous_index]

            if open_sign == 1 and low_sign == 1:
                if open_price < close_price and close_price > open_price_previous:
                    point = close_price
                else:
                    point = low_price
            elif open_sign == -1 and low_sign == -1:
                point = low_price
            elif open_sign == -1 and low_sign == 1:
                if close_price < close_price_previous:
                    point = low_price
                else:
                    point = close_price
            elif open_sign == 1 and low_sign == -1:
                if open_price < close_price:
                    point = close_price
                else:
                    point = low_price
            else:
                point = low_price

            points = np.append(points, point)

        self._pattern_points = points

    def _set_markers_and_mean_points(self):
        direction = None
        prev_direction = None
        values = np.array([])
        markers_index = []
        points = self._pattern_points

        for (idx, point) in np.ndenumerate(points):
            index = idx[0]
            value_to_insert = None

            if direction is None:
                direction = 'up'
            else:
                if index < points.size - 1:
                    if points[index] > points[index + 1]:
                        direction = 'down'
                    else:
                        direction = 'up'

            if (direction != prev_direction) and index < points.size - 1:
                value_to_insert = points[index]
                markers_index.append(index)
            elif index < points.size - 2:
                value_to_insert = (points[index] + points[index + 1]) / 2
            else:
                value_to_insert = points[index]

            values = np.append(values, value_to_insert)
            prev_direction = direction

        self._markers_index = markers_index
        self._pattern_mean_points = values

    def get_w_patterns(self) -> list:
        markers_index = self._markers_index
        points = self._pattern_mean_points

        w = []
        for (i, marker_x) in enumerate(markers_index[:-1]):
            plotable = i < len(markers_index) - 3
            next_marker_x = markers_index[i + 1]

            if plotable:
                # is it a "W"
                if (
                    points[marker_x] > points[markers_index[i + 1]] and
                    points[markers_index[i + 1]] < points[markers_index[i + 2]] and
                    points[markers_index[i + 2]] > points[markers_index[i + 3]] and points[markers_index[i + 3]] > points[markers_index[i + 1]] and
                    points[markers_index[i + 4]] > points[markers_index[i + 2]]
                ):
                    w.extend([markers_index[i:i + 5]])

        self._w_pattern = w
        return self._w_pattern
