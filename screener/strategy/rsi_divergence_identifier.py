import numpy as np

from screener.enums.pivot_type import PivotType
from screener.services.datafeed.datafeed_handler import DatafeedHandler


class RSIDivergenceIdentifier:
    def __init__(self, datafeed_handler: DatafeedHandler) -> None:
        self._datafeed_handler = datafeed_handler
        df = datafeed_handler._stock_dataframe
        df['RSI_pivot'] = df.apply(
            lambda row: self._identify_pivot_points(df, row.name, 5, 5),
            axis=1
        )
        df['RSI_pivot_marker'] = df.apply(
            lambda row: self._mark_identified_pivot_points_position(
                df, row, 5, 5),
            axis=1
        )

    def _identify_pivot_points(self, df, current, range_before, range_after):
        if current-range_before < 0 or current+range_after >= len(df):
            return 0

        is_local_low = True
        is_local_high = True

        for i in range(current-range_before, current+range_after+1):
            if (df.RSI[current] > df.RSI[i]):
                is_local_low = False
            if (df.RSI[current] < df.RSI[i]):
                is_local_high = False

        if is_local_low and is_local_high:
            return PivotType.lowHigh.value
        elif is_local_low:
            return PivotType.low.value
        elif is_local_high:
            return PivotType.high.value
        else:
            return PivotType.none.value

    def _mark_identified_pivot_points_position(self, df, current, range_before, range_after):
        if current['RSI_pivot'] == PivotType.low.value:
            return current['RSI'] - 1
        elif current['RSI_pivot'] == PivotType.high.value:
            return current['RSI'] + 1
        else:
            return np.nan
