import random
from numpy import ndarray
from pandas import DataFrame
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots


class WPatternDrawer:
    def __init__(
        self,
        dataframe: DataFrame,
        chart_line: ndarray,
        identified_patterns: list
    ):
        self._dataframe = dataframe
        self._chart_line = chart_line
        self._identified_patterns = identified_patterns

    # METHODS

    def draw(self, figure):
        # Draws full
        # figure.add_trace(
        #     go.Scatter(x=self._dataframe.index[1:], y=self._chart_line, mode='lines')
        # )

        patterns_count = len(self._identified_patterns)
        i = 0

        while i < patterns_count:
            figure.append_trace(
                go.Scatter(
                    x=self._dataframe.index[self._identified_patterns[i]],
                    y=self._chart_line[self._identified_patterns[i]],
                    text=f"W pattern n°{i+1}"
                ),
                row=1,
                col=1,
            )
            i += 1
