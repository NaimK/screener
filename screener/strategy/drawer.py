import plotly.graph_objects as go

from screener.services.datafeed.datafeed_handler import DatafeedHandler
from screener.strategy.fibo_drawer import FiboDrawer
from screener.strategy.price_drawer import PriceDrawer
from screener.strategy.rsi_drawer import RSIDrawer
from screener.strategy.rsi_divergence_identifier import RSIDivergenceIdentifier
from screener.strategy.w_pattern_drawer import WPatternDrawer
from screener.strategy.w_pattern_identifier import WPatternIdentifier


class Drawer:
    def __init__(
        self,
        datafeed_handler: DatafeedHandler
    ):
        self._datafeed_handler = datafeed_handler
        self._fibo_drawer = None
        self._price_drawer = None
        self._rsi_drawer = None
        self._w_pattern_drawer = None
        self._prepare_figure()
        self._prepare_drawers()

    def _prepare_figure(self):
        self._figure = go.Figure()
        self._figure.set_subplots(rows=2, cols=1)

    def _prepare_drawers(self):
        dfh = self._datafeed_handler

        # Identify signals
        rsi_divergence_identifier = RSIDivergenceIdentifier(dfh)
        w_pattern_identifier = WPatternIdentifier(dfh)
        w_pattern_identifier.look_for_pattern()

        # Draw
        self._w_pattern_drawer = WPatternDrawer(
            dfh.stock_dataframe,
            w_pattern_identifier._pattern_mean_points,
            w_pattern_identifier.get_w_patterns()
        )
        self._fibo_drawer = FiboDrawer(dfh.stock_dataframe)
        self._price_drawer = PriceDrawer(dfh.stock_dataframe)
        self._rsi_drawer = RSIDrawer(dfh.stock_dataframe)

    def draw_indicators(self):
        self._price_drawer.draw(self._figure)
        self._w_pattern_drawer.draw(self._figure)
        self._fibo_drawer.draw(self._figure)
        self._rsi_drawer.draw(self._figure)
        # Remove Rangeslider
        self._figure.update_layout(xaxis_rangeslider_visible=False)
        # Show plot
        self._figure.show()
