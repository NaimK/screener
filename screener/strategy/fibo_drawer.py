from pandas import DataFrame


class FiboDrawer:
    def __init__(self, dataframe: DataFrame):
        self._dataframe = dataframe
        self.price_min = self._dataframe['close'].min()
        self.price_max = self._dataframe['close'].max()
        self.level1 = 0
        self.level2 = 0
        self.level3 = 0
        self.diff = 0
        self.get_fibo_levels()

    def get_fibo_levels(self):
        self.diff = self.price_max - self.price_min
        self.level1 = self.price_max - 0.382 * self.diff
        self.level2 = self.price_max - 0.618 * self.diff
        self.level3 = self.price_max - 0.786 * self.diff

    def draw(self, figure):
        figure.add_hline(self.price_min, opacity=0.2, annotation_text="1")
        figure.add_hline(self.level3, opacity=0.2, annotation_text="0.786")
        figure.add_hline(self.level2, opacity=0.2, annotation_text="0.618")
        figure.add_hline(self.level1, opacity=0.2, annotation_text="0.382")
        figure.add_hline(self.price_max, opacity=0.2, annotation_text="0")

        figure.add_hrect(
            y0=self.level3,
            y1=self.level2,
            line_width=0,
            fillcolor="orange",
            opacity=0.2
        )
