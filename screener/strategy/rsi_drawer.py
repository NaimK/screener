from pandas import DataFrame
import plotly.graph_objects as go


class RSIDrawer:
    def __init__(
        self,
        dataframe: DataFrame,
    ):
        self._dataframe = dataframe

    # METHODS

    def draw(self, figure):
        self._draw_rsi(figure)
        self._draw_rsi_divergences(figure)

    def _draw_rsi(self, figure):
        figure.append_trace(
            go.Scatter(
                x=self._dataframe.index,
                y=self._dataframe['RSI']
            ),
            row=2,
            col=1,
        )

    def _draw_rsi_divergences(self, figure):
        df = self._dataframe

        figure.append_trace(
            go.Scatter(
                x=df.index,
                y=df['RSI_pivot_marker'],
                mode="markers",
                marker=dict(size=5, color="MediumPurple"),
                name="pivot"
            ),
            row=2,
            col=1,
        )
