from pandas import DataFrame
import plotly.graph_objects as go


class PriceDrawer:
    def __init__(
        self,
        dataframe: DataFrame,
    ):
        self._dataframe = dataframe

    # METHODS

    def draw(self, figure):
        df = self._dataframe

        figure.append_trace(
            go.Candlestick(
                x=df.index,
                open=df['open'],
                high=df['high'],
                low=df['low'],
                close=df['close'],
            ),
            row=1,
            col=1,
        )
